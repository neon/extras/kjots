kjots (4:6.0.0-0neon) noble; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Tue, 02 Jul 2024 06:14:22 +0000

kjots (4:5.1.1-0neon) noble; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Thu, 20 Apr 2023 23:45:07 +0000

kjots (4:5.1.0-4) unstable; urgency=medium

  [ Patrick Franz ]
  * Team upload.
  * Add patch to allow building against KDE PIM 22.04.

 -- Patrick Franz <deltaone@debian.org>  Tue, 28 Jun 2022 14:52:45 +0200

kjots (4:5.1.0-3) unstable; urgency=medium

  [ Patrick Franz ]
  * Team upload.
  * Add patches to allow building against KDE PIM 21.12.

 -- Patrick Franz <deltaone@debian.org>  Wed, 30 Mar 2022 20:10:57 +0200

kjots (4:5.1.0-2) unstable; urgency=medium

  [ Norbert Preining ]
  * Remove Max from uploaders, thanks for your work.
  * Add myself to uploaders.
  * Use Debian Qt/KDE as maintainer.
  * Bump standards version, no changes necessary.

 -- Norbert Preining <norbert@preining.info>  Tue, 31 Aug 2021 09:26:34 +0900

kjots (4:5.1.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Update path in watch file, and bump to version 4.
  * Update the build dependencies according to the upstream build system:
    - bump KF packages to 5.71.0
    - add libkf5libkdepim-dev
    - drop libkf5textwidgets-dev, and xsltproc, no more required
    - explicitly add qtbase5-dev
  * Bump Standards-Version to 4.5.1, no changes required.
  * Update the patches:
    - apps2008-compat-upstream-bcf49fb9.patch: drop, backported from upstream
  * Stop shipping the upstream README as documentation, as it does not provide
    much of useful information.
  * Update/rewrite copyright.
  * Update lintian overrides.
  * Update the references to the upstream Git repository in
    debian/upstream/metadata.

 -- Pino Toscano <pino@debian.org>  Sun, 21 Feb 2021 11:31:01 +0100

kjots (4:5.0.2-3) unstable; urgency=medium

  * Team upload.

  [ Norbert Preining ]
  * Fix compilation with KDE Apps 20.08 (Closes: #972226)

  [ Pino Toscano ]
  * Bump the debhelper compatibility to 13:
    - switch the debhelper-compat build dependency to 13
  * Remove the explicit as-needed linking, as it is done by binutils now.
  * Add Rules-Requires-Root: no.
  * Bump Standards-Version to 4.5.0, no changes required.
  * Set fields Upstream-Name, Upstream-Contact in debian/copyright.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata (already
    present in machine-readable debian/copyright).

 -- Pino Toscano <pino@debian.org>  Fri, 23 Oct 2020 13:36:05 +0200

kjots (4:5.0.2-2) unstable; urgency=medium

  * Team upload.
  * Switch Vcs-* fields to salsa.debian.org.
  * Add the configuration for the CI on salsa.
  * Drop the kde-l10n breaks/replaces, no more needed after two Debian stable
    releases.
  * Switch from dhmk to the dh sequencer:
    - invoke the dh sequencer using the kf5 addon
    - explicitly link in as-needed mode
  * Bump the debhelper compatibility to 12:
    - switch the debhelper build dependency to debhelper-compat 12
    - remove debian/compat
  * Use https for Format in copyright, and in the watch file.
  * Bump Standards-Version to 4.4.1, no changes required.
  * Explicitly add the gettext build dependency.

 -- Pino Toscano <pino@debian.org>  Sun, 08 Dec 2019 09:11:35 +0100

kjots (4:5.0.2-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Update watch file.
  * Replace deprecated build dependencies:
    - kio-dev -> libkf5kio-dev
  * Add the libkf5textwidgets-dev build dependency, new requirement.
  * Bump Standards-Version to 4.0.0, no changes required.

 -- Pino Toscano <pino@debian.org>  Fri, 23 Jun 2017 20:56:23 +0200

kjots (4:5.0.1-2) unstable; urgency=medium

  * Release to unstable.

 -- Maximiliano Curia <maxy@debian.org>  Tue, 05 Jul 2016 13:55:01 +0200

kjots (4:5.0.1-1~) experimental; urgency=medium

  * New upstream release (5.0.1).
  * Update copyright information
  * Update build-deps and deps with the info from cmake
  * Install all the files
  * Add missing build dep, xsltproc
  * Add kde-l10n scripted breaks replaces for kjots.mo

 -- Maximiliano Curia <maxy@debian.org>  Fri, 24 Jun 2016 16:05:21 +0200
