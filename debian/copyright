Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: kjots

Files: *
Copyright: 1997, Christoph Neerfeld <Christoph.Neerfeld@home.ivm.de>
           2002-2003, Aaron J. Seigo <aseigo@kde.org>
           2003, Stanislav Kljuhhin <crz@hot.ee>
           2005-2006, Jaison Lee <lee.jaison@gmail.com>
           2007-2008, Stephen Kelly <steveire@gmail.com>
           2009, Montel Laurent <montel@kde.org>
           2016, Daniel Vrátil <dvratil@kde.org>
           2020, Igor Poboiko <igor.poboiko@gmail.com>
License: GPL-2+

Files: src/kjotsmodel.cpp
       src/kjotsmodel.h
       src/kjotswidget.cpp
       src/kjotswidget.h
       src/noteshared/notecreatorandselector.cpp
       src/noteshared/notecreatorandselector.h
       src/noteshared/notelockattribute.cpp
       src/noteshared/notelockattribute.h
       src/noteshared/notepinattribute.cpp
       src/noteshared/notepinattribute.h
       src/noteshared/standardnoteactionmanager.cpp
       src/noteshared/standardnoteactionmanager.h
       src/notesortproxymodel.cpp
       src/notesortproxymodel.h
       src/uistatesaver.cpp
       src/uistatesaver.h
Copyright: 1997, Christoph Neerfeld <Christoph.Neerfeld@home.ivm.de>
           2002-2003, Aaron J. Seigo <aseigo@kde.org>
           2003, Stanislav Kljuhhin <crz@hot.ee>
           2005-2006, Jaison Lee <lee.jaison@gmail.com>
           2007-2009, Stephen Kelly <steveire@gmail.com>
           2008, Volker Krause <vkrause@kde.org>
           2009-2010, Tobias Koenig <tokoe@kde.org>
           2010, Klarälvdalens Datakonsult AB
           2020, Igor Poboiko <igor.poboiko@gmail.com>
License: LGPL-2+

Files: data/org.kde.kjots.appdata.xml
Copyright: 2015, Dan Vrátil <dvratil@kde.org>
           2016, Luca Beltrame <lbeltrame@kde.org>
           2016-2017, Luigi Toscano <luigi.toscano@tiscali.it> <ltoscano@redhat.com>
License: CC0-1.0

Files: po/*
Copyright: 1998, 2002-2005, Erik Kjær Pedersen <erik@binghamton.edu>
           1998, Bjarni R. Einarsson <bre@mmedia.is>
           1998, Denis Pershin <dyp@perchine.com>
           1998-2000, Hans Petter Bieker <bieker@kde.org>
           1999, 2003-2004, Hasso Tepper <hasso@linux.ee> <hasso@estpak.ee> <hasso@kde.org>
           1999, Dillion Chen <dillon.chen@turbolinux.com.cn>
           1999, Jañ-Mai Drapier <jdrapier@club-internet.fr>
           1999, Sári Gábor <saga@mail.externet.hu>
           1999-2002, Meni Livne <livne@kde.org>
           2000, Adem GUNES <adem@alaeddin.cc.selcuk.edu.tr>
           2000, Damjan Janevski <miopa@usa.net>
           2000, Marko Samastur <markos@elite.org>
           2000-2002, Pablo de Vicente <vicente@oan.es>
           2001, Kim jaehwan <myri7@yahoo.co.kr>
           2001, Waseem Bakr <bakr@mit.edu>
           2001-2002, Frikkie Thirion <frix@expertron.co.za>
           2001-2002, Lorint Hendschel <LorintHendschel@skynet.be>
           2001-2009, Free Software Foundation, Inc.
           2002, 2004, Stanislav Visnovsky <visnovsky@nenya.ms.mff.cuni.cz> <visnovsky@kde.org>
           2002, A.L. Klyutchenya <asoneofus@kde.ru>
           2002, Funda Wang <fundawang@en2china.com>
           2002, Fundile Majola <fundile@translate.org.za>
           2002, Knut Yrvin <knut.yrvin@gmail.com>
           2002, Lwandle Mgidlana <lwandle@translate.org.za>
           2002, Mario Teijeiro Otero <mteijeiro@escomposlinux.org>
           2002, Noboru Sinohara <shinobo@leo.bekkoame.ne.jp>
           2002, Ričardas Čepas <rch@richard.eu.org>
           2002, Ömer Fadıl USTA <omer_fad@hotmail.com>
           2002-2003, Stergios Dramis <sdramis@egnatia.ee.auth.gr>
           2002-2003, Xiong Jiang <jxiong@offtopic.org>
           2003, 2005, 2009, Andrey Cherepanov <skull@kde.ru>
           2003, 2008, Teemu Rytilahti <teemu.rytilahti@d5k.net> <teemu.rytilahti@kde-fi.org>
           2003, Antonio Sergio de Mello e Souza <asergioz@bol.com.br>
           2003, Delafond <gerard@delafond.org>
           2003, Görkem Çetin <gorkem@kde.org>
           2003, Lisiane Sztoltz <lisiane@conectiva.com.br>
           2003, Marcos  <marcos@euskalgnu.org>
           2003, Nuriddin Aminagha <nuriddin@eminaga.de>
           2003, Pablo Saratxaga <pablo@walon.org>
           2003, Pjetur G. Hjaltason <pjetur@pjetur.net>
           2003, Thanomsub Noppaburana <donga_n@yahoo.com>
           2003-2004, Eloy Cuadra <eloihr2@eresmas.com>
           2003-2004, Gonzalo H. Castilla <<ttxzgl@yahoo.es>>
           2003-2004, Robert Jacolin <rjacolin@ifrance.com>
           2003-2005, 2007, Gregor Zumstein <zumstein@ssd.ethz.ch> <gz@orchester-bremgarten.ch>
           2003-2005, 2009, Malcolm Hunter <malcolm.hunter@gmx.co.uk>
           2003-2005, Gregor Rakar <gregor.rakar@kiss.uni-lj.si> <gregor.rakar@kiss.si>
           2004, 2008-2010, 2012, Bjørn Steensrud <bjornst@skogkatt.homelinux.org>
           2004, Björgvin Ragnarsson <nifgraup@hotmail.com>
           2004, Claudiu Costin <claudiuc@kde.org>
           2004, Dilshod Marupov <dma165@hotmail.com>
           2004, Engin Çağatay <engincagatay@yahoo.com>
           2004, Heiko Evermann <heiko@evermann.de>
           2004, Nickolai Shaforostoff <shafff@ukr.net>
           2004, SATOH Satoru <ss@kde.gr.jp>
           2004, Tapio Kautto <eleknader@phnet.fi>
           2004, Þröstur Svanbergsson <throstur@bylur.net>
           2004-2005, 2007, 2011, Fumiaki Okushi <okushi@kde.gr.jp>
           2004-2005, 2007-2009, 2016, 2019-2020, Eloy Cuadra <ecuadra@eloihr.net>
           2004-2005, 2009, 2020, Iñigo Salvador Azurmendi <xalba@euskalnet.net>
           2004-2005, 2009-2010, Andrew Coles <andrew_coles@yahoo.co.uk>
           2004-2005, Federico Cozzi <federicocozzi@federicocozzi.it>
           2004-2005, Lisiane Sztoltz Teixeira <lisiane@conectiva.com.br>
           2004-2006, Bozidar Proevski <bobibobi@freemail.com.mk>
           2005, 2007-2010, 2012, 2014, 2020, Marek Laane <bald@smail.ee>
           2005, 2008, Andrej Vernekar <andrej.vernekar@moj.net>
           2005, 2008-2010, Thomas Reitelbach <tr@erdfunkstelle.de>
           2005, Axel Bojer <fri_programvare@bojer.no>
           2005, Bekir SONAT <cortexbs@yahoo.com>
           2005, Erik K. Pedersen <erik@binghamton.edu>
           2005, Henrique Pinto <henrique.pinto@kdemail.net>
           2005, Ion Gaztañaga <igaztanaga@gmail.com>
           2005, Kenshi Muto <kmuto@debian.org>
           2005, MIMOS Open Source <opensource@mimos.my>
           2005, Matthieu Robin <kde@macolu.org> <matthieu@macolu.org>
           2005, Nils Kristian Tomren <slx@nilsk.net>
           2005, Stephan Johach <hunsum@gmx.de>
           2005-2006, 2008-2012, Nicola Ruggero <nixprog.adsl@tiscali.it> <nicola@nxnt.org>
           2005-2006, Ilpo Kantonen <ilpo@iki.fi>
           2005-2008, Spiros Georgaras <sng@hellug.gr> <sngeorgaras@otenet.gr>
           2005-2009, Donatas Glodenis <dgvirtual@akl.lt>
           2006, 2009, 2012, Jure Repinc <jlp@holodeck1.com>
           2006, 2009, Richard Fric <Richard.Fric@kdemail.net>
           2006, 2011-2012, Sairan Kikkarin <sairan@computer.org>
           2006, Abdurahmonov Nurali <mavnur@gmail.com>
           2006, Khaled Hosny <dr.khaled.hosny@gmail.com>
           2006, Mahesh Subedi <submanesh@yahoo.com>
           2006, MaryamSadat Razavi <razavi@itland.ir>
           2006, Mashrab Kuvatov <kmashrab@uni-bremen.de>
           2006, Nasim Daniarzadeh <daniarzadeh@itland.ir>
           2006, Xabi G. Feal <xabigf@gmx.net>
           2006-2007, Nazanin Kazemi <kazemi@itland.ir>
           2006-2007, Zlatko Popov <zlatkopopov@fsa-bg.org>
           2006-2009, 2014, Sönke Dibbern <s_dibbern@web.de>
           2006-2010, Frank Weng (a.k.a. Franklin) <franklin at goodhorse dot idv dot tw>
           2007, 2010, 2012-2013, Franklin Weng <franklin@goodhorse.idv.tw>
           2007, Darafei Praliaskouski <komzpa@licei2.com>
           2007, Fahad Al-Saidi <fahad.alsaidi@gmail.com>
           2007, Maris Nartiss <maris.kde@gmail.com>
           2007, Mikko Piippo <piippo@cc.helsinki.fi>
           2007, Nabin Gautam <nabin@mpp.org.np>
           2007, Narayan Kumar Magar <narayan@mpp.org.np>
           2007, Rihards Prieditis <RPrieditis@inbox.lv>
           2007, Siarhei Liantsevich <serzh.by@gmail.com>
           2007, Tahereh Dadkhahfar <dadkhahfar@itland.ir>
           2007, Youssef Chahibi <chahibi@gmail.com>
           2007, shyam krishna bal <shyamkrishna_bal@yahoo.com>
           2007-2008, Guillaume Pujol <guill.p@gmail.com>
           2007-2008, Yannig Marchegay (Kokoyaya) <yannig@marchegay.org>
           2007-2008, mvillarino <mvillarino@users.sourceforge.net>
           2007-2009, 2013-2015, 2019-2020, Shinjo Park <kde@peremen.name>
           2007-2009, Toussis Manolis <manolis@koppermind.homelinux.org>
           2007-2010, Lie_Ex <lilith.ex@gmail.com>
           2007-2010, Yukiko Bando <ybando@k6.dion.ne.jp>
           2008, 2010, 2012, Nicolas Ternisien <nicolas.ternisien@gmail.com>
           2008, 2011, Jean Cayron <jean.cayron@gmail.com> <jean.cayron@base.be>
           2008, Anne-Marie Mahfouf <annma@kde.org>
           2008, Auk Piseth <piseth_dv@khmeros.info>
           2008, Franco Mariluis <fmariluis@gmail.com>
           2008, Jan Madsen <jan.madsen.pt@gmail.com>
           2008, Leonardo Finetti <finex@finex.org>
           2008, Luciano Montanaro <mikelima@cirulla.net>
           2008, Sébastien Renard <Sebastien.Renard@digitalfox.org>
           2008, Σπύρος Γεωργαράς <sngeorgaras@otenet.gr>
           2008-2009, 2012, 2014, Sergiu Bivol <sergiu@cip.md>
           2008-2009, 2012-2013, 2016, 2020, Burkhard Lück <lueck@hube-lueck.de>
           2008-2009, Ravishankar Shrivastava <raviratlami@yahoo.com> <raviratlami@aol.in>
           2008-2009, obsoleteman <tulliana@gmail.com>
           2008-2010, 2012, Xosé <xosecalvo@gmail.com>
           2008-2010, Eirik U. Birkeland <eirbir@gmail.com>
           2008-2010, Manfred Wiese <m.j.wiese@web.de>
           2008-2010, Viesturs Zarins <viesturs.zarins@mii.lu.lv>
           2008-2011, 2013-2015, 2018, André Marcelo Alvarenga <alvarenga@kde.org>
           2008-2011, 2021, Karl Ove Hufthammer <karl@huftis.org>
           2008-2012, 2014, 2016, 2020, Martin Schlander <mschlander@opensuse.org>
           2009, Dario Andres Rodriguez <andrebajotierra@gmail.com>
           2009, Kevin Scannell <kscanne@gmail.com>
           2009, Nick Shaforostoff <shafff@ukr.net>
           2009-2010, Panagiotis Papadopoulos <pano_90@gmx.net>
           2009-2014, Javier Viñal <fjvinal@gmail.com>
           2010, 2019-2020, Luiz Fernando Ranghetti <elchevive@opensuse.org>
           2010, 2020, Tommi Nieminen <translator@legisign.org>
           2010, A S Alam <aalam@users.sf.net>
           2010, David Kolibac <david@kolibac.cz>
           2010, Giorgos Katsikatsos <giorgos.katsikatsos@gmail.com>
           2010, H. İbrahim Güngör <ibrahim@pardus.org.tr>
           2010, Rajesh Ranjan <rajesh672@gmail.com>
           2010-2011, 2013-2015, Lasse Liehu <lasse.liehu@gmail.com>
           2010-2011, 2014, 2016, Alexander Potashev <aspotashev@gmail.com>
           2010-2011, 2016, Frederik Schwarzer <schwarzer@kde.org>
           2010-2013, 2015-2017, 2020, Vít Pelčák <vit@pelcak.org>
           2011, 2012, 2013, 2018, 2020, g.sora <g.sora@tiscali.it>
           2011, 2014, Feng Chao <rainofchaos@gmail.com>
           2011, Kristóf Kiszel <ulysses@kubuntu.org>
           2011, Remigijus Jarmalavičius
           2011, Sahran <sahran.ug@gmail.com>
           2011, Torbjörn Klatt <torbjoern@torbjoern-klatt.de>
           2012, 2014, Balázs Úr <urbalazs@gmail.com>
           2012, Dimitrios Glentadakis <dglent@gmail.com>
           2012, Julia Dronova <juliette.tux@gmail.com>
           2012, Marcus Gama <marcus.gama@gmail.com>
           2012, Sophea <sophea@khmeros.info>
           2012-2013, 2015-2016, Roman Paholík <wizzardsk@gmail.com>
           2012-2013, Marce Villarino <mvillarino@kde-espana.es>
           2012-2013, Weng Xuetian <wengxt@gmail.com>
           2013, 2020, xavier <xavier.besnard@neuf.fr>
           2013, Alexander Lakhin <exclusion@gmail.com>
           2013, Chetan Khona <chetan@kompkin.com>
           2013, Liudas Ališauskas <liudas@akmc.lt>
           2013, Sebastien Renard <renard@kde.org>
           2013, Volkan Gezer <volkangezer@gmail.com>
           2013, Yasen Pramatarov <yasen@lindeas.com>
           2014, Kaan Ozdincer <kaanozdincer@gmail.com>
           2015, 2017-2019, Adrián Chaves Fernández (Gallaecio) <adrian@chaves.io> <adriyetichaves@gmail.com>
           2015, Mindaugas Baranauskas <opensuse.lietuviu.kalba@gmail.com>
           2015-2016, 2020, Freek de Kruijf <freekdekruijf@kde.nl>
           2015-2016, 2020, Stefan Asserhäll <stefan.asserhall@bredband.net>
           2015-2016, Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>
           2016, 2020, Steve Allewell <steve.allewell@gmail.com>
           2016, Petros Vidalis <pvidalis@gmail.com>
           2017, Elkana Bardugo <ttv200@gmail.com>
           2017, Jeff Huang <s8321414@gmail.com>
           2017, Stelios <sstavra@gmail.com>
           2017, scootergrisen
           2018, 2020, Simon Depiets <sdepiets@gmail.com>
           2019, JungHee Lee <daemul72@gmail.com>
           2019, Victor Ibragimov <victor.ibragimov@gmail.com>
           2020, Matjaž Jeran <matjaz.jeran@amis.net>
           2020, Vincenzo Reale <smart2128@baslug.org>
License: GPL-2+

Files: po/ca/kjots.po
       po/ca@valencia/kjots.po
       po/uk/kjots.po
Copyright: 2000, 2002, Andriy Rysin <rysin@kde.org>
           2004, Ivan Petrouchtchak <iip@telus.net>
           2005, 2007, Ivan Petrouchtchak <ivanpetrouchtchak@yahoo.com>
           2008, 2015, 2016, 2020, Yuri Chornoivan <yurchor@ukr.net>
           2008, Ivan Petrouchtchak <fr.ivan@ukrainian-orthodox.org>
           2014, 2015, 2016, 2020, Antoni Bella Pérez <antonibella5@yahoo.com>
           2015, 2016, 2020, Josep Ma. Ferrer <txemaq@gmail.com>
License: LGPL-2.1-3-KDEeV

Files: debian/*
Copyright: 2016, Debian/Kubuntu Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
           2012, Pino Toscano <pino@debian.org>
License: GPL-2+

License: CC0-1.0
 On Debian systems, the complete text of the CC0 Public Domain Dedication
 can be found in "/usr/share/common-licenses/CC0-1.0".

License: GPL-2+
 On Debian systems, the complete text of the GNU General Public License
 version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: LGPL-2+
 On Debian systems, the complete text of the GNU Library General Public License
 version 2 can be found in "/usr/share/common-licenses/LGPL-2".

License: LGPL-2.1-3-KDEeV
 This file is distributed under the license LGPL version 2.1 or
 version 3 or later versions approved by the membership of KDE e.V.
 .
 On Debian systems, the complete texts of GNU Library General Public License
 version 2.1 and 3 can be found in "/usr/share/common-licenses/LGPL-2.1" and
 "/usr/share/common-licenses/LGPL-3".
